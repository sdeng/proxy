var SimpleReverseProxy = require('simple-reverse-proxy');


new SimpleReverseProxy([
    // Add your backend server port below.
    // 'http://localhost:8080',
    'http://localhost:2524'
], {
    agent: false
})
.listen(10000);
